﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ColorBasedOnSpeed : MonoBehaviour {

    protected Rigidbody rb;
    protected Renderer render;
	// Use this for initialization
	void Start () {
        rb = GetComponent<Rigidbody>();
        render = GetComponent<Renderer>();
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    void FixedUpdate()
    {
        Vector3 vel = rb.velocity;
        float speed = vel.magnitude;
        float intensity = Mathf.Clamp((speed - 5.0f) / 5.0f, 0.0f, 1.0f);

        render.material.color = new Color(intensity, 1.0f - intensity, 1.0f - intensity);
        

    }
}
