﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ColorOnCollision : MonoBehaviour {
    public Color altColor = Color.black;
    public Renderer rend;
    private TrailRenderer trail;
    // Use this for initialization
    void Start () {
        //Get the renderer of the object so we can access the color
        rend = GetComponent<Renderer>();
        //trail = GetComponent<TrailRenderer>();
    }
	
	// Update is called once per frame
	void Update () {
		
	}

    private void OnCollisionEnter(Collision collision)
    {
        altColor.r = 1.0f;
        altColor.g = 0.0f;
        altColor.b = 0.0f;
        rend.material.color = altColor;
        //trail.material.SetColor("_TintColor", altColor);
    }

    private void OnCollisionExit(Collision collision)
    {
        altColor.r = 1.0f;
        altColor.g = 1.0f;
        altColor.b = 1.0f;
        rend.material.color = altColor;
        //trail.material.SetColor("_TintColor", altColor);
    }
}
