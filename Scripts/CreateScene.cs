﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CreateScene : MonoBehaviour
{
    public Transform [] prefabs;
    public uint xslices = 10;
    public uint yslices = 3;
    public uint zslices = 10;
    public Color[] colors;

    void Start()
    {
        for (int i = 0; i < xslices; i++)
        {
            for (int j = 0; j < zslices; j++)
            {
                for (int k = 0; k < yslices; k++)
                {
                    int idx = Random.Range(0, prefabs.Length);
                    Transform obj = Instantiate(prefabs[idx], new Vector3(i - 4.5f, 5.0f - (yslices / 2) + k, j - 4.5f), Quaternion.identity);
                    //idx = Random.Range(0, colors.Length);
                    //obj.GetComponent<Renderer>().material.color = colors[idx];
                }
            }
        }
    }

    public void dropobject(float y = 10.0f)
    {
        for (int i = 0; i < xslices; i++)
        {
            for (int j = 0; j < zslices; j++)
            {
                
            
                int idx = Random.Range(0, prefabs.Length);
                Transform obj = Instantiate(prefabs[idx], new Vector3(i - 4.5f, 5.0f - (yslices / 2) + y, j - 4.5f), Quaternion.identity);
                idx = Random.Range(0, colors.Length);
                obj.GetComponent<Renderer>().material.color = colors[idx];
                
            }
        }
    }
    // Update is called once per frame
    void Update()
    {
    }
}
