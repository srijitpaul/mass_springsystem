﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class moveball : MonoBehaviour
{
    public float m_Radius = 2.0f;
    private float theta = 0.0f;
    private Vector3 m_CurrentPos = new Vector3(0.0f, 0.0f, 0.0f);
    public float m_AnglePerSecond = 90.0f;
    protected Rigidbody rb;
    private float scale = 1.0f;


    // Use this for initialization
    void Start()
    {
        m_CurrentPos = new Vector3(0.0f, 0.5f, 0.0f);
        this.GetComponent<Transform>().position = m_CurrentPos;
        rb = GetComponent<Rigidbody>();
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        float dt = Time.deltaTime;
        m_CurrentPos.x = m_Radius * Mathf.Cos(theta);
        m_CurrentPos.z = m_Radius * Mathf.Sin(theta);
        m_CurrentPos.y = 1.5f + m_Radius * Mathf.Sin(1.5f * theta) * Mathf.Cos(theta);

        theta += Mathf.Deg2Rad * m_AnglePerSecond * dt;
        if (theta >= 2.0f * Mathf.PI)
            theta -= 2.0f * Mathf.PI;
        //GetComponent<Transform>().position = m_CurrentPos;
        //GetComponent<Transform>().Rotate(Vector3.left, 5.0f);
        rb.MovePosition(m_CurrentPos);
        rb.AddTorque(transform.up * 50.0f, ForceMode.Force);
    }

    public void scaleBall(float s)
    {
        scale = s;
        GetComponent<Transform>().localScale = new Vector3(s, s, s);
    }

    public void radius_rotation(float rot)
    {
        m_Radius = rot;
    }
}