﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RotateObject : MonoBehaviour {
    public float m_Amount = 100.0f;
    private Rigidbody rb;
	// Use this for initialization
	void Start () {
        rb = GetComponent<Rigidbody>();
	}

    private void FixedUpdate()
    {
        float h = Input.GetAxis("Horizontal") * m_Amount * Time.deltaTime;
        float v = Input.GetAxis("Vertical") * m_Amount * Time.deltaTime;

        rb.AddTorque(transform.up * h);
        rb.AddTorque(transform.right * v);
    }
}
