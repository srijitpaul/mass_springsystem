﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Push : MonoBehaviour {
    public float amount = 1000.0f;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    private void OnMouseDown()
    {
        //Debug.Log("Key Pressed!");
        GetComponent<Rigidbody>().AddForce(Vector3.forward * amount, ForceMode.Force);
    }

	private void OnMouseExit()
	{
		GetComponent<Transform>().localScale *= 1.1f;
		GetComponent<Rigidbody>().mass *= 1.1f;
	}
}
